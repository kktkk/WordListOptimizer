#!/usr/bin/env python

import sys
from urllib import FancyURLopener
from bs4 import BeautifulSoup

class MyOpener(FancyURLopener):
    version = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11'

mopen = MyOpener()

try:
    import gi

    gi.require_version("Gtk", "3.0")
    from gi.repository import Gtk
    from gi.repository import GObject as gobject
except:
    print "GTK3.0 not found, please verify your system."
    pass
    try:
        from gtk import gtk as Gtk
        import gobject
    except:
        print "GTK not found, please verify your system."
        sys.exit(1)

WORDS_MINIMUM_SIZE = 8


class Wordlist_GTK:

    def __init__(self):
        self.builder = Gtk.Builder()  # create an instance of the gtk.Builder
        self.builder.add_from_file('WordlistOptim.glade')  # add the xml file to the Builder
        self.window_welcome = self.builder.get_object("WelcomeWindow")
        self.window_main = self.builder.get_object("MainWindow")
        self.about = self.builder.get_object("AboutWindow")
        self.welcome = self.builder.get_object("WelcomeWindow")
        self.window_file = self.builder.get_object("FileWindow")
        self.window_url = self.builder.get_object("UrlWindow")
        self.entry_url = self.builder.get_object("entry_url")
        self.result = self.builder.get_object("result_box")
        self.file_selected = None
        self.url_selected = None
        self.builder.connect_signals(self)
        self.window_welcome.show()

    def on_MainWindow_destroy(self, object, data=None):
        print "Main Window quit."
        Gtk.main_quit()

    def on_WelcomeWindow_destroy(self, object, data=None):
        print "Welcome Window quit."
        Gtk.main_quit()

    def on_button_about_clicked(self, *args):
        print "About Selected"
        self.response = self.about.run()
        self.about.hide()

    def on_button_src_url_clicked(self, *args):
        print "Url Picker Selected"
        self.window_welcome.hide()
        self.response = self.window_url.run()
        self.window_url.hide()

    def on_button_src_file_clicked(self, *args):
        print "File Picker Selected"
        self.window_welcome.hide()
        self.response = self.window_file.run()
        self.file_selected = self.window_file.get_filename()
        self.window_file.hide()

    def on_button_file_select_clicked(self, *args):
        print "File Selected"
        self.window_file.hide()
        self.window_main.show()

    def on_button_file_cancel_clicked(self, *args):
        print "File Picker Cancel Button clicked "
        self.window_file.hide()
        self.window_welcome.run()

    def on_button_url_select_clicked(self, *args):
        print "Url Selected"
        self.url_selected = self.entry_url.get_text()
        self.window_url.hide()
        self.window_main.show()

    def on_button_url_cancel_clicked(self, *args):
        print "URL Picker Cancel Button clicked "
        self.window_url.hide()
        self.window_welcome.run()

    def on_button_main_start_clicked(self, *args):
        print "Process started"
        if self.file_selected:
            print self.file_selected
            f = open(self.file_selected, 'r').read()
            wordlist = ''
            for words in f.split(' '):
                words = words.replace('\t', '')
                words = words.replace('\n', '')
                if len(words) >= WORDS_MINIMUM_SIZE:
                    wordlist += words + ' ' + "\n"
            self.result.set_text(wordlist)
            f.close()
        elif self.url_selected:
            print self.url_selected
            soup = BeautifulSoup(mopen.open(self.url_selected).read(), "html5lib")
            list_of_words = soup.get_text().split(' ')
            wordlist = ''
            for words in list_of_words:
                words = words.replace('\t', '')
                words = words.replace('\n', '')
                if len(words) >= WORDS_MINIMUM_SIZE:
                    wordlist += words + ' ' + "\n"
            self.result.set_text(wordlist)

    def on_button_main_save_clicked(self, *args):
        print 'Save selected'
        result = self.result.get_text()
        f = open('wordlist.txt', 'w')
        f.write(result)
        f.close()
        Gtk.main_quit()


if __name__ == "__main__":
    main = Wordlist_GTK()  # create an instance of our class
    Gtk.main()  # run the thing